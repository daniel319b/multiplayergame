﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.IO;
using Microsoft.Xna.Framework;

namespace MultiplayerGameEngine
{
    public delegate void RecievedPacket(Packet p);

    public class GameClient
    {
        TcpClient client;
        byte[] readBuffer;
        int BUFFER_SIZE = 2048;

        MemoryStream readStream, writeStream;
        BinaryReader reader;
        BinaryWriter writer;

        public GameClient()
        {
            client = new TcpClient();
            client.NoDelay = true;
            client.Connect("localhost", 86);
            readBuffer = new byte[BUFFER_SIZE];

            readStream = new MemoryStream();
            writeStream = new MemoryStream();
            reader = new BinaryReader(readStream);
            writer = new BinaryWriter(writeStream);

            NetworkController.Client = client;
            WaitForData();
        }

        private void WaitForData()
        {
            client.GetStream().BeginRead(readBuffer, 0, BUFFER_SIZE, proccessStream, null);
        }


        private void proccessStream(IAsyncResult ar)
        {
            int bytesRead = 0;
            lock (client.GetStream()){ bytesRead = client.GetStream().EndRead(ar); }

            //bad connection, close it
            if (bytesRead == 0)
            {
                client.Close();
                //display a message.
                return;
            }

            //got data, read it.
            byte[] data = new byte[bytesRead];

            for (int i = 0; i < data.Length; i++)
                data[i] = readBuffer[i];

            ProccessData(data);

            WaitForData();
        }

        private void ProccessData(byte[] data)
        {
            //convert the byte array data to a memory stream for easier reading.
            //the structure of the data is [protocol(byte), data,data..., protocol, data,data...]
            readStream = new MemoryStream(data,0,data.Length);
            readStream.Position = 0; 
            reader = new BinaryReader(readStream);

            Protocol p = (Protocol)reader.ReadByte();
            if (p == Protocol.PlayerConnected )
            {
                PlayerConnectionPacket packet = new PlayerConnectionPacket("Connected");
                packet.Parse(reader);
                
                if(!enemyConnected) PlayerConnected(packet);
                enemyConnected = true;
                //let the player that connected know that this player is connected too.
                //send a message to the server saying "hey i'm (TcpClient client) connected too"
                if (packet.Status.Equals("Notify"))
                    NotifyOtherPlayers();
            }
            else if (p == Protocol.PlayerDisconnected)
            {
                Packet packet = new PlayerConnectionPacket("Disconnected");
                packet.Parse(reader);
                PlayerDisconnected(packet);
                enemyConnected = false;
            }
            else if(p == Protocol.PlayerMoved)
            {
               
                PlayerMovedPacket packet = new PlayerMovedPacket();
                packet.Parse(reader);
                PlayerMoved(packet);
            }
        }

        private void NotifyOtherPlayers()
        {
           
            writeStream.Position = 0;
            writer.Write((byte)Protocol.PlayerConnected);
            NetworkController.SendDataToServer(writeStream);
        }

       
        //packets events
        public event RecievedPacket PlayerConnected;
        public event RecievedPacket PlayerDisconnected;
        public event RecievedPacket PlayerMoved;

        public bool enemyConnected;
    }
    
}
