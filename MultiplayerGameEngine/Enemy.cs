﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MultiplayerGameEngine
{
    public class Enemy : GameObject
    {

        public Enemy()
        {
            Position = new Vector2(130,30);
        }

        public override void Draw(SpriteBatch sb)
        {
            sb.Draw(texture, position, Color);
        }

    }
}
