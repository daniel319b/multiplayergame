﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MultiplayerGameEngine
{
    public enum Protocol
    {
        PlayerDisconnected = 0,
        PlayerConnected = 1,      
        PlayerMoved = 2,
    }
}
