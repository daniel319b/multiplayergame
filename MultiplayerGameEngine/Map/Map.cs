﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace MultiplayerGameEngine
{
    public class Map : TileMap
    {
        [ContentSerializerIgnore]
        public Player Player { get; set; }

    
        public Map()
        {
           
        }

        public void Update(GameTime gameTime)
        {
            
            Player.Update(gameTime);
          
        }

       
    }
}
