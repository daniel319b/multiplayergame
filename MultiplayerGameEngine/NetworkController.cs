﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.IO;

namespace MultiplayerGameEngine
{
    public class NetworkController
    {
        public static TcpClient Client { get; set; }

        public static void SendPacketToServer(Packet p)
        {
            SendDataToServer(p.GetStream());
        }

        public static void SendDataToServer(MemoryStream ms)
        {
            SendDataToServer(GetDataFromMemoryStream(ms));
        }

        public static void SendDataToServer(byte[] data)
        {
            try
            {
                lock (Client.GetStream())
                {
                    Client.GetStream().BeginWrite(data, 0, data.Length, null, null);
                }
            }
            catch (Exception e)
            {
                //show the error
            }

        }

        private static byte[] GetDataFromMemoryStream(MemoryStream ms)
        {
            byte[] result;
            lock (ms)
            {
                int bytesWritten = (int)ms.Position;
                result = new byte[bytesWritten];

                ms.Position = 0;
                ms.Read(result, 0, bytesWritten);
            }

            return result;
        }

    }
}
