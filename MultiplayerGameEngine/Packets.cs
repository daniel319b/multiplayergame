﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.Xna.Framework;

namespace MultiplayerGameEngine
{
    /// <summary>
    /// A Base packet class, holds only 2 fields: ID and IP
    /// </summary>
    public class Packet
    {
        protected MemoryStream dataStream;
        protected BinaryWriter writer;

        public string IP { get; set; }

        public byte ID { get; set; }

        public Packet()
        {
            dataStream = new MemoryStream();
            writer = new BinaryWriter(dataStream);
        }

        public MemoryStream GetStream()
        {
            if(dataStream.Length == 0) WriteDataToStream();
            return dataStream;
        }

        /// <summary>
        /// Parses a memory stream into a base packet: only IP and ID.
        /// </summary>
        /// <param name="reader"></param>
        public virtual void Parse(BinaryReader reader)
        {
            ID = reader.ReadByte();
            IP = reader.ReadString();
        }

        protected virtual void WriteDataToStream() { }
    }

    /// <summary>
    /// A packet that holds the connection data about a client
    /// </summary>
    public class PlayerConnectionPacket : Packet
    {
        public string Status { get; set; }

        public PlayerConnectionPacket(string status)
        {
            Status = status;
        }  
    }

    /// <summary>
    /// A packet that holds the data of the event that a player moved
    /// </summary>
    public class PlayerMovedPacket : Packet
    {
        public Vector2 DeltaPosition { get; set; }

        protected override void WriteDataToStream()
        {
            writer.Write((byte)Protocol.PlayerMoved);
            writer.Write(DeltaPosition.X);
            writer.Write(DeltaPosition.Y);
        }

        public override void Parse(BinaryReader reader)
        {
            float dx = reader.ReadSingle();
            float dy = reader.ReadSingle();
            DeltaPosition = new Vector2(dx, dy);
            base.Parse(reader);
        }
    }
}
