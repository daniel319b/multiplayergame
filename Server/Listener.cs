﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;

namespace Server
{
    public class Listener
    {
        TcpListener listener;
        private bool[] usedUserID;
        int maxNumberOfClients = 2;

        /// <summary>
        /// Fires up when a new client has connected.
        /// </summary>
        public event ConnectionEvent ClientAdded;

        /// <summary>
        /// Makes a new Listener with a specified port to listen to.
        /// </summary>
        /// <param name="port">A port to listen to.</param>
        public Listener(int port)
        {
            listener = new TcpListener(IPAddress.Any, port);
            usedUserID =new  bool[maxNumberOfClients];
        }

        /// <summary>
        /// Starts Listening for new client connections.
        /// </summary>
        public void Start()
        {
            listener.Start();
            ListenForClients();
        }

        /// <summary>
        /// Stops the Listener.
        /// </summary>
        public void Stop()
        {
            listener.Stop();
        }

        /// <summary>
        /// Listens to new Connections.
        /// </summary>
        private void ListenForClients()
        {
            Console.WriteLine("Waiting For New Connections...\n");
            listener.BeginAcceptTcpClient(AcceptClientConnection, null);
        }

        private void AcceptClientConnection(IAsyncResult ar)
        {
            
            TcpClient client = listener.EndAcceptTcpClient(ar);//accept a client.
            //id is originally -1 which means a user cannot connect
            int id = -1;
            for (byte i = 0; i < usedUserID.Length; i++)
            {
                if (usedUserID[i] == false)
                {
                    id = i;
                    break;
                }
            }

            //If the id is still -1, the client what wants to connect cannot (probably because we have reached the maximum number of clients
            if (id == -1)
            {
                Console.WriteLine("Client {0} cannot connect, Server is full! ", client.Client.RemoteEndPoint.ToString());
                return;
            }
            //ID is valid, so create a new Client object with the server ID and IP
            usedUserID[id] = true;
            Client newClient = new Client(client,(byte) id);//make a new client with the currnet ID and TcpClient.
            newClient.Disconnecting += Client_Disconnected;//hook up the disconnected event.

            //Yay! Client Connected!

            if(ClientAdded != null)
                ClientAdded(this,newClient);//Call the Client Added event.
            ListenForClients();//Continue listening.
        }

        void Client_Disconnected(object sender,Client client)
        {
            usedUserID[client.ID] = false;
        }
    }
}
