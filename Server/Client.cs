﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;

namespace Server
{
    public class Client
    {
        public event ConnectionEvent Disconnecting;

        public readonly byte ID;
        public string IP;

        private TcpClient client;
        private bool connected;
        private int ReadBufferSize = 2048;
        private byte[] readBuffer;

        public event DataReceivedEvent DataReceived;

        public Client(TcpClient client, byte currentClientID)
        {
            readBuffer = new byte[ReadBufferSize];
            this.client = client;
            IP = client.Client.RemoteEndPoint.ToString();
            ID = currentClientID;
            client.NoDelay = true;
            connected = true;
            StartListening();
        }

        /// <summary>
        /// Disconnects this Client.
        /// </summary>
        public void Disconnect()
        {
            if (connected)
            {
                connected = false;
                client.Close();//Closes the connection.

                //Call the event
                if (Disconnecting != null)
                    Disconnecting(this, this);
            }
        }

        /// <summary>
        /// Listents to new Data.
        /// </summary>
        private void StartListening()
        {
            client.GetStream().BeginRead(readBuffer, 0, ReadBufferSize, RecieveData, null);
        }

        private void RecieveData(IAsyncResult ar)
        {
            int bytesRecieved;
            try
            {
                lock (client.GetStream())
                {
                    bytesRecieved = client.GetStream().EndRead(ar);
                }
            }
            catch (Exception e)
            {
                Disconnect();
                Console.WriteLine("Client {0}:  Bad Connection -  Disconnecting...", IP);
                return;
            }

            if (bytesRecieved == 0)
            {
                Disconnect();
                Console.WriteLine("Client {0}:  Bad Connection -  Disconnecting...", IP);
                return;
            }
            byte[] data = new byte[bytesRecieved];

            for (int i = 0; i < bytesRecieved; i++)
                data[i] = readBuffer[i];
            StartListening();
            if (DataReceived != null)
                DataReceived(this, data);
        }

        public void SendData(byte[] data)
        {
            try
            {
                lock (client.GetStream())
                {

                    client.GetStream().BeginWrite(data, 0, data.Length, null, null);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.ReadLine();
            }

        }

        public override string ToString()
        {
            return IP;
        }
    }
}
