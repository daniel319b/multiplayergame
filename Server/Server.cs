﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Server
{
    public enum Protocol
    {
        UserDisconnected = 0,
        UserConnected = 1,
    }

    public class Server
    {
        Listener listener;
        Client[] clients;
        int maxNumberOfClients = 2;

        public int ClientsConnected { get; private set; }

        MemoryStream readStream;
        MemoryStream writeStream;
        BinaryReader reader;
        BinaryWriter writer;

        public Server(int port)
        {
            clients = new Client[maxNumberOfClients];
            listener = new Listener(port);
            listener.ClientAdded += listener_ClientAdded;
            listener.Start();

            readStream = new MemoryStream();
            writeStream = new MemoryStream();
            reader = new BinaryReader(readStream);
            writer = new BinaryWriter(writeStream);
        }

        void listener_ClientAdded(object sender, Client client)
        {
            ClientsConnected++;
            clients[client.ID] = client;
            client.DataReceived += client_DataReceived;
            client.Disconnecting += client_Disconnecting;

            writeStream.Position = 0;
            //(Protocol,client ID, client IP)
            writer.Write((byte)Protocol.UserConnected);
            writer.Write(client.ID);
            writer.Write(client.IP);;

            SendToAll(writeStream, client);
            Console.WriteLine("Client {0} IP: {1} Connected!", client.ID, client.IP);
        }

       

        void client_Disconnecting(object sender, Client client)
        {
            ClientsConnected--;

            writeStream.Position = 0;

            writer.Write((byte) Protocol.UserDisconnected);
            writer.Write(client.ID);
            writer.Write(client.IP);

            SendToAll(writeStream, client);

            Console.WriteLine("Client {0} IP: {1} Disconnected!", client.ID, client.IP);
            clients[client.ID] = null;
        }

        void client_DataReceived(Client sender, byte[] data)
        {
            writeStream.Position = 0;

            //Append the id and IP of the original sender to the message, and combine the two data sets.
            writer.Write(sender.ID);
            writer.Write(sender.IP);
            data = CombineData(data, writeStream);

            SendToAll(data, sender);
        }

        private byte[] CombineData(byte[] data, MemoryStream ms)
        {
            //Get the byte array from the MemoryStream
            byte[] result = GetDataFromMemoryStream(ms);

            //Create a new array with a size that fits both arrays
            byte[] combinedData = new byte[data.Length + result.Length];

            //Add the original array at the start of the new array
            for (int i = 0; i < data.Length; i++)
            {
                combinedData[i] = data[i];
            }

            //Append the new message at the end of the new array
            for (int j = data.Length; j < data.Length + result.Length; j++)
            {
                combinedData[j] = result[j - data.Length];
            }

            //Return the combined data
            return combinedData;
        }

        private byte[] GetDataFromMemoryStream(MemoryStream ms)
        {
            byte[] result;

            //Async method called this, so lets lock the object to make sure other threads/async calls need to wait to use it.
            lock (ms)
            {
                int bytesWritten = (int)ms.Position;
                result = new byte[bytesWritten];

                ms.Position = 0;
                ms.Read(result, 0, bytesWritten);
            }

            return result;
        }


        private void SendToAll(MemoryStream memoryStreamData, Client sender)
        {
            byte[] result;

            lock (memoryStreamData)
            {
                int bytesWritten = (int)memoryStreamData.Position;
                result = new byte[bytesWritten];

                memoryStreamData.Position = 0;
                memoryStreamData.Read(result, 0, bytesWritten);
            }
            SendToAll(result, sender);

        }

        private void SendToAll(byte[] data, Client sender)
        {
            foreach (Client c in clients)
                if (c != null && c != sender)
                    c.SendData(data);
            writeStream.Position = 0;
        }
    }
}
