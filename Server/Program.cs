﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
namespace Server
{
    class Program
    {
        static TcpClient client;
        static byte[] readBuffer = new byte[2048];

        static void Main(string[] args)
        {
            int port = 86;

            Server server = new Server(port);
            
            while (true)
            {
                System.Threading.Thread.Sleep(1000);
            }
            //TcpListener server = new TcpListener(IPAddress.Any, port);

            ////Start listening.
            //server.Start();

            ////Listening Loop
            //while (true)
            //{
            //    Console.WriteLine("Waiting for connections... ");

            //    //Gets the current connected client.
            //    client = server.AcceptTcpClient();
            //    Console.WriteLine(client.Client.LocalEndPoint.ToString() + " Connected!");

            //    try
            //    {
            //        SendToClient("Server Sends Hello");
            //        while (true)
            //        {
            //            StartListening();
            //            string msg = Console.ReadLine();
            //            SendToClient(msg);
            //        }
            //    }
            //    catch (Exception e)
            //    {
            //        Console.WriteLine(e.ToString());
            //        Console.ReadLine();
            //    }

             
            //}
            //Console.ReadLine();
        }




        static void SendToClient(string msg)
        {
            byte[] buffer = Encoding.Default.GetBytes(msg);
            client.GetStream().Write(buffer, 0, msg.Length);
            client.GetStream().Flush();

        }


        static void StartListening()
        {
            client.GetStream().BeginRead(readBuffer, 0, readBuffer.Length, StreamRecieved, null);
        }

        static void StreamRecieved(IAsyncResult ar)
        {
            int bytesRead;
            lock (client.GetStream())
            {
                bytesRead = client.GetStream().EndRead(ar);
            }
            byte[] data = new byte[bytesRead];

            for (int i = 0; i < bytesRead; i++)
                data[i] = readBuffer[i];

            string msg = Encoding.Default.GetString(data, 0, bytesRead);
            Console.WriteLine("Client Sent: " + msg);
            StartListening();

        }
    }
}
