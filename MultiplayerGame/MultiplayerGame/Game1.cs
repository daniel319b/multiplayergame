using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Net.Sockets;
using MultiplayerGameEngine;

namespace MultiplayerGame
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        GameClient gameClient;
        Player player;
        Enemy enemy;
        TileMap map;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            graphics.PreferredBackBufferHeight = 710;
            graphics.PreferredBackBufferWidth = 1280;
            IsMouseVisible = true;
        }

        protected override void Initialize()
        {
            player = new Player();
            enemy = new Enemy();
            map = new TileMap();

            base.Initialize();
        }

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            TextureManager.Content = Content;
           
            player.Texture = TextureManager.SetTexture("player");

            
            enemy.Texture = TextureManager.SetTexture("player");
            enemy.Color = Color.Blue;

            gameClient = new GameClient();
            gameClient.PlayerConnected += new RecievedPacket(gameClient_PlayerConnected);
            gameClient.PlayerDisconnected += new RecievedPacket(gameClient_PlayerDisconnected);
            gameClient.PlayerMoved += new RecievedPacket(gameClient_PlayerMoved);

            map = Content.Load<TileMap>("Map2");
            player.Map = map;
            map.DrawCollisionLayer = true;
        }

        void gameClient_PlayerMoved(Packet p)
        {
            PlayerMovedPacket pa = p as PlayerMovedPacket;
            //enemy.Position = new Vector2(enemy.Position.X + pa.DeltaPosition.X, enemy.Position.Y - pa.DeltaPosition.Y);
            enemy.Position = pa.DeltaPosition;
        }

        void gameClient_PlayerDisconnected(Packet p)
        {
            enemy = null;
        }

        void gameClient_PlayerConnected(Packet p)
        {
            PlayerConnectionPacket pa = p as PlayerConnectionPacket;
            if (gameClient.enemyConnected) return; 

            NetworkController.SendPacketToServer(new PlayerMovedPacket() { DeltaPosition = player.Position });

            pa.Status = "Notify";
        }
      
        protected override void Update(GameTime gameTime)
        {
            Input.Update();
            if (player != null) player.Update(gameTime);
            if (gameClient.enemyConnected && enemy != null)  enemy.Update();

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            map.Draw(spriteBatch);
            spriteBatch.Begin();
            if(player != null) player.Draw(spriteBatch);
            if(gameClient.enemyConnected) enemy.Draw(spriteBatch);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
